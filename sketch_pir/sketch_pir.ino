#define LEDPIN 13
#define PIRPIN 2

void setup()
{
    pinMode(LEDPIN, OUTPUT);
    digitalWrite(LEDPIN, LOW);
    
    pinMode(PIRPIN, INPUT);
    digitalWrite(PIRPIN, HIGH); // pullup    
    
    Serial.begin(38400);
}

void loop()
{
    if (digitalRead(PIRPIN) == HIGH)
    {
        digitalWrite(LEDPIN, HIGH);
        Serial.println("dale!");
        delay(15000);
    }
    else
    {
        digitalWrite(LEDPIN, LOW);
        delay(5000);
    }
}

