#!/usr/bin/env python
import os, time, sys
import threading
import random
import RPi.GPIO as GPIO
import serial

# CONFIGURATION

PATTERN_PATH = "./"
PATTERN_FILE = "patterns.txt"
SERIAL_PORT = "/dev/ttyUSB0"

WAIT_FOR_SERIAL = False
LOOP_FOREVER = True
REPEATS = 1

# GLOBALS

current_state = [0,0,0,0,0,0,0,0,0,0,0,0,0]

# FUNCTIONS

def turn_on(group):
#    GPIO.output(gpios[group], True)
#    time.sleep(random.uniform(0.1,0.3))
#    GPIO.output(gpios[group], False)
#    time.sleep(random.uniform(0.1,0.3))
#    GPIO.output(gpios[group], True)
#    time.sleep(random.uniform(0.1,0.3))
#    GPIO.output(gpios[group], False)
#    time.sleep(random.uniform(0.1,0.3))
    GPIO.output(gpios[group], True)

def turn_off(group):
    GPIO.output(gpios[group], False)

def kitt_test():
	for j in range (1):
		for i in range(13):
			GPIO.output(gpios[i], True)
			if i > 0:
				GPIO.output(gpios[i-1], False)
			time.sleep(1)
		
		for i in range(11, 0, -1):
			GPIO.output(gpios[i], True)
			GPIO.output(gpios[i+1], False)
			time.sleep(1)

def all_test():
	for i in range(13):
		GPIO.output(gpios[i], True)
	time.sleep(5)
	for i in range(13):
		GPIO.output(gpios[i], False)

def all_off():
    for i in range(13):
        GPIO.output(gpios[i], False);

# MAIN

# Configure GPIOs
GPIO.setwarnings(False)
gpios = [5,6,12,13,16,17,18,19,26,21,22,23,24]
GPIO.setmode(GPIO.BCM)
for g in gpios:
	GPIO.setup(g, GPIO.OUT)

# open patterns file
try:
	input_file = open(PATTERN_PATH+"/"+PATTERN_FILE, "r")
except Exception as e:
	print("Problem loading file: " + str(e))
	sys.exit(1)

# open serial port
if WAIT_FOR_SERIAL:
	try:
		ser=serial.Serial("/dev/ttyUSB0", 9600)
	except Exception as e:
		print("Problem opening serial port: " + str(e))
		sys.exit(1)


# test all the tubes
kitt_test()
all_test()

while 1:
    try:
        if WAIT_FOR_SERIAL:
                while ser.readline().strip() != "dale!":
                        pass

        condition = ""

        if LOOP_FOREVER:
            condition = "True"
        else:
            condition = "i < REPEATS"

        i = 0
        while eval(condition):
            # read line
            line = input_file.readline()
            while line != "":
                    # get time and next state
                    line_elements = line.split(",")
                    pause = float(line_elements[0])
                    next_state = []
                    for i in range(13):
                        next_state.append(int(line_elements[i+1]))

                    # make changes
                    for i in range(13):
                        if current_state[i] == 0 and next_state[i] == 1:
                            t = threading.Thread(target=turn_on, args=(i,))
                            t.start()
                            current_state[i] = 1;
                        if current_state[i] == 1 and next_state[i] == 0:
                            turn_off(i)
                            current_state[i] = 0;
             
                    # make indicated pause
                    time.sleep(pause)
                    # get next line
                    line = input_file.readline()

            # turn off all the tubes
            all_off()

            # rewind
            input_file.seek(0) 
            i = i + 1

        #  clean serial buffer
        ser.flushInput()

    except KeyboardInterrupt:
            print("\nInterrupted!")
            try:
                    input_file.close()
            except NameError:
                    pass
            
            # Close GPIOs
            GPIO.cleanup()
            # Close serial port
            ser.close()


